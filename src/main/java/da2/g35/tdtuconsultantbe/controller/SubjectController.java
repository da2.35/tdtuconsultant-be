package da2.g35.tdtuconsultantbe.controller;

import da2.g35.tdtuconsultantbe.entity.Subject;
import da2.g35.tdtuconsultantbe.service.SubjectService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/subject")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    @GetMapping(value = "/combination/{id}")
    public ResponseEntity<List<Subject>> getSubjectsByCombination(@Valid @PathVariable Long id){
        List<Subject> subjects = subjectService.getSubjectsByCombination(id);
        return ResponseEntity.ok(subjects);
    }
}
